/* execute
    $ cp audioc_template.c audioc.c

    Then, replace FILL comments with real code.
 */

/** Includes of the project **/
#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <math.h>
#include <sys/soundcard.h>
#include <sys/ioctl.h>
#include "audiocArgs.h"
#include "circularBuffer.h"
#include "configureSndcard.h"
#include "rtp.h"

/** Constants **/
//Minimum amplitude for silence detection
#define MA_8 4
#define MA_16 (MA_8 * 4)
//Silence depending on the format
#define SILENCE_8 128
#define SILENCE_16 0
//Percentage of Minimum Amplitude to consider recorded packets as silences
#define PMA 80

#define TIME_10_SECONDS 10
#define TIME_TIMER 10

/** Global variables **/
//Soundcard buffer
char *record_buffer = NULL;
//Socket buffer
char *socket_buffer = NULL;
//Buffer for circular buffer
char *circularBuffer = NULL;
//The following packet contains the RTP header and the data to be sent
void *paquete_red = NULL;
// Silence buffer
void *silence_buffer = NULL;
//The following variable is intended to keep apart the RTP header from the received packets
rtp_hdr_t *rtp_header_received = NULL;
/** To determine the remaining time **/
int current_blocks_in_cb = 0;
/** Some configuration cte values **/
int sample_size; int rate; int numberOfBlocks_in_cb;int requestedFragmentSize;
int channelNumber = 1;

/** Define function **/
void record (int descSnd, int fragmentSize);
void play (int descSnd, int fragmentSize);
void calculate_remaining_time(int sndCardDesc, struct timeval *remaining_time);
void print_verbose();
double floor(double x);

bool isFragmentSilence(char *buffer);

/**
 * VERBOSE VARIABLES
 */
bool verbose;
/** Time variables **/
uint32_t packetDuration = 0;
struct timeval first_read_time;
struct timeval first_playout;
int is_first_playout = 0;
struct timeval last_playout;
/** Recorded variables **/
int recorded_packets = 0;
int silences_recorded = 0;
int packets_sent = 0;
/** Playout variables **/
int inserted_packets = 0;
int silences_detected = 0;
int losts_detected = 0;
int packets_received = 0;

int first_packet_received;





/** RTP seq and timestamp **/
uint16_t local_seq = 0;
uint32_t local_ts = 0;

uint16_t external_seq = 0;
uint32_t external_ts=0;

/** Ctrl+C functionality**/
void signalHandler (int sigNum __attribute__ ((unused)))  /* __attribute__ ((unused))   -> this indicates gcc not to show an 'unused parameter' warning about sigNum: is not used, but the function must be declared with this parameter */
{
    printf ("\naudioC was requested to finish\n");
    //if (record_buffer) free(record_buffer);
    //if (fileName) free(fileName);
    if (socket_buffer) free(socket_buffer);
    if(paquete_red) free(paquete_red);
    //if(rtp_header_received) free(rtp_header_received);
    if(silence_buffer) free(silence_buffer);
    cbuf_destroy_buffer(circularBuffer);
    if(verbose) {
        print_verbose();
    }
    exit (0);
}

void print_verbose(){
    int theorical_time = (inserted_packets *requestedFragmentSize * 1000 / (rate * 1 * (sample_size / 8)));
    theorical_time = theorical_time > 0 ? theorical_time : -theorical_time;
    struct timeval actual_time;
    timersub(&last_playout, &first_playout, &actual_time);
    printf("################Times################\n");
    printf("Theorical time played: %d ms \n", theorical_time);
    printf("Actual time played: %ld ms \n",actual_time.tv_usec / 1000 + actual_time.tv_sec * 1000);
    printf("################Recording process################\n");
    printf("Total number of packets recorded: %d\n",recorded_packets);
    printf("Total number of packets sent: %d\n",packets_sent);
    printf("Total number of silences detected: %d\n",silences_recorded);
    printf("################Playout process################\n");
    printf("Total number of packets played: %d\n",inserted_packets);
    printf("Total number of silences detected: %d\n",silences_detected);
    printf("Total number of losts detected: %d\n",losts_detected);
    printf("Total number of packets received: %d\n", packets_received);
}


/** Main functionality **/
int main(int argc, char *argv[]) {
    struct sigaction sigInfo;
    struct in_addr multicastIp;
    uint32_t ssrc;
    uint16_t port;
    uint8_t vol;
    uint32_t bufferingTime; /* in milliseconds */
    uint8_t payload;

    int sndCardFormat;


    /** State variable when the application is initialized
    * Used to retain blocks while receiving packets into the circular buffer **/
    int number_of_block = 0;
    int sndCardDesc;


    /** Setup rtp header variables **/
    rtp_hdr_t *rtp_header = NULL;
    //rtp_header_received = malloc(12);

    /** Obtains values from the command line - or default values otherwise **/
    if (args_capture_audioc(argc, argv, &multicastIp, &ssrc,
                            &port, &vol, &packetDuration, &verbose, &payload, &bufferingTime) == EXIT_FAILURE)
    { exit(1);  /* there was an error parsing the arguments, error info
                   is printed by the args_capture function */
    };


    /** Installs signal **/
    sigInfo.sa_handler = signalHandler;
    sigInfo.sa_flags = 0;
    sigemptyset(&sigInfo.sa_mask); /* clear sa_mask values */
    if ((sigaction (SIGINT, &sigInfo, NULL)) < 0) {
        printf("Error installing signal, error: %s", strerror(errno));
        exit(1);
    }

    /** Configures sound card **/
    channelNumber = 1;
    if (payload == PCMU) {
        rate = 8000;
        sndCardFormat = U8;
    }else if (payload == L16_1) {
        rate = 8000; // cambia
        sndCardFormat = 16; // cambia
    }
    requestedFragmentSize = (packetDuration * rate)/1000;

    /* configures sound card, sndCardDesc is filled after it returns */
    configSndcard (&sndCardDesc, &sndCardFormat, &channelNumber, &rate, &requestedFragmentSize, true);
    vol = configVol (channelNumber, sndCardDesc, vol);


    /** Create and configure circular buffer */
    numberOfBlocks_in_cb = (bufferingTime+200) / packetDuration;

    circularBuffer = (char *) cbuf_create_buffer(numberOfBlocks_in_cb, requestedFragmentSize);
    int numberOfBlocksToPlay;
    double div = bufferingTime/packetDuration;
    numberOfBlocksToPlay = floor (div);
    //+1 to store '/0' at the end.
    socket_buffer = malloc(requestedFragmentSize + 12);

    /** Configuring the socket **/

    struct sockaddr_in remToSendSAddr, remToRecvSAddr;
    struct ip_mreq mreq;
    int socketDesc;

    socklen_t sockAddrInLength; /* to store the length of the address returned by recvfrom */
    sockAddrInLength = sizeof(struct sockaddr_in); /* remember always to set the size of the rem variable in from_len */

    /* Configures socket */
    bzero(&remToSendSAddr, sizeof(remToSendSAddr));
    remToSendSAddr.sin_family = AF_INET;
    remToSendSAddr.sin_port = htons(port);
    remToSendSAddr.sin_addr = multicastIp;


    /* Creates socket */
    if ((socketDesc = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
        printf("socket error\n");
        exit(EXIT_FAILURE);
    }

    /* configure SO_REUSEADDR, multiple instances can bind to the same multicast address/port */
    int enable = 1;
    if (setsockopt(socketDesc, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0) {
        printf("setsockopt(SO_REUSEADDR) failed");
        exit(EXIT_FAILURE);
    }

    if (bind(socketDesc, (struct sockaddr *)&remToSendSAddr, sizeof(struct sockaddr_in)) < 0) {
        printf("bind error\n");
        exit(EXIT_FAILURE);
    }

    /* setsockopt configuration for joining to mcast group */
    mreq.imr_multiaddr = multicastIp;
    mreq.imr_interface.s_addr = htonl(INADDR_ANY);
    if (setsockopt(socketDesc, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq, sizeof(mreq)) < 0) {
        printf("setsockopt error");
        exit(EXIT_FAILURE);
    }

    /* Do not receive packets sent to the mcast address by this process */
    unsigned char loop=0;
    setsockopt(socketDesc, IPPROTO_IP, IP_MULTICAST_LOOP, &loop, sizeof(unsigned char));


    /**Define sample size **/
    sample_size = payload == PCMU ? 8: 16;
    int frame_packet = (requestedFragmentSize * 8) / sample_size;

    /**Generate a silence fragment to play it on the soundcard**/
    silence_buffer = malloc(requestedFragmentSize);
    //DONE: what if sample_size is 16? -> The 0 level would be then at int16_t = 0, so we do not need to feel again the silence_buffer (already done by calloc).

    if (payload == PCMU) {
        uint8_t* silence_pointer = (uint8_t* ) silence_buffer;
        int i;
        for (i = 0; i < requestedFragmentSize; i++) {
            *silence_pointer = SILENCE_8;
            silence_pointer = silence_pointer + 1;
        }
    } else if (payload == L16_1) {
        int16_t* silence_pointer = (int16_t*) silence_buffer;
        int i;
        for (i = 0; i < requestedFragmentSize/2; i++) {
            *silence_pointer  =  SILENCE_16;
            silence_pointer = silence_pointer + 1;
        }
    }
    /** Configuring set and timer functions **/

    fd_set read_block, write_block;
    struct timeval time;
    int result;
    int ready;

    //We need the timer to be 10ms
    time.tv_sec = 0; //seconds
    time.tv_usec = 10000; //microseconds


    paquete_red = malloc((size_t)(12+requestedFragmentSize));


    //first_playout variable changes to 0 after starting the playout for the very first time
    //int first_playout = 1;

    struct timeval current_time, diff_time;

    first_read_time.tv_sec = 0;
    first_read_time.tv_usec = 0;

    current_time.tv_sec = 0;
    current_time.tv_usec = 0;

    diff_time.tv_sec = 0;
    diff_time.tv_usec = 0;

    //To know how many bytes are left at the soundcard
    //  int bytes_exhausted;
    //  int remaining_msecs = 10; //10ms as default value

    //The same for the circularBuffer
    //  int bytes_exhausted_circular;
    //  int remaining_msecs_circular = 0;

    //We configure remaining time default value as 20ms so that remaining_time - ten_ms_time = 10ms as default value
    struct timeval remaining_time;
    remaining_time.tv_sec = 0;
    remaining_time.tv_usec = 0;

    struct timeval ten_ms_time;
    ten_ms_time.tv_sec = 0;
    ten_ms_time.tv_usec = TIME_TIMER * 1000;

    //Configure timers
    int first_10_secs = 1;
    //Get current time
    /** First loop **/
    while(number_of_block < numberOfBlocksToPlay) {
        FD_ZERO(&read_block);
        FD_ZERO(&write_block);
        FD_SET(sndCardDesc, &read_block);
        FD_SET(socketDesc, &read_block);
        ready = select(FD_SETSIZE, &read_block, &write_block, NULL, NULL);
        if (ready <= 0) {

        } else {
            if (FD_ISSET(sndCardDesc, &read_block) == 1) {
                if (first_10_secs) {
                    first_10_secs = 0;
                    gettimeofday(&first_read_time, NULL);
                }
                gettimeofday(&current_time, NULL);
                timersub(&current_time, &first_read_time, &diff_time);
                //DONE se empieza desde el cero en ts y seq o no hace falta? -> sí
                //Data is available to be sent to the multicast address.
                //Now we should fill in the RTP header
                rtp_header = (rtp_hdr_t *) paquete_red;
                rtp_header->version = RTP_VERSION; //Protocol version
                rtp_header->p = 0; //Padding flag
                rtp_header->x = 0; //Header extension flag
                rtp_header->cc = 1; //CSRC count
                rtp_header->m = 0; //Marker bit
                rtp_header->pt = payload; //Payload type.
                rtp_header->seq = htons(local_seq);
                rtp_header->ts = htonl(local_ts);
                rtp_header->ssrc = htonl(ssrc);
                record_buffer = (char *) (rtp_header + 1);
                record(sndCardDesc, requestedFragmentSize);
                /* Using sendto to send information. Since I've bind the socket, the local (source)
                 * port of the packet is fixed. In the rem structure I set the remote (destination) address and port  */

                bool isSilence = (diff_time.tv_sec >= TIME_10_SECONDS) && isFragmentSilence(record_buffer);

                if (!isSilence) {
                    if ((sendto(socketDesc, paquete_red, requestedFragmentSize + 12, /* flags*/  0,
                                (struct sockaddr *) &remToSendSAddr, sizeof(remToSendSAddr))) < 0) {
                        printf("sendto error\n");
                    } else {
                        if(verbose) {
                            packets_sent++;
                            printf(".");
                            fflush(stdout);
                        }
                        local_seq++;
                    }
                } else {
                    if(verbose) {
                        silences_recorded++;
                        printf("_");
                        fflush(stdout);
                    }
                    //Even if it is a silence, we must update the TS:

                }
                local_ts += frame_packet;
                fflush(stdout);
                memset(paquete_red, 0, (size_t) (12 + requestedFragmentSize));
            }

            if (FD_ISSET(socketDesc, &read_block) == 1) {
                //Data from another audioc entity has arrived at the socket.
                if ((result = recvfrom(socketDesc, socket_buffer, requestedFragmentSize + 12, 0,
                                       (struct sockaddr *) &remToRecvSAddr, &sockAddrInLength)) < 0) {
                    printf("recvfrom error\n");
                } else {
                    //printf("Audio received\n");
                    if(verbose) {
                        printf("+");
                        fflush(stdout);
                        packets_received++;
                    }

                   // socket_buffer[result] = 0; /* convert to 'string' by appending a 0 value (equal to '\0') after the last received character */


                    char* pointer_to_write;
                    if ((pointer_to_write = cbuf_pointer_to_write(circularBuffer)) != NULL) {
                        //TODO: pointer_to_write could be NULL if there are no free slots at the buffer (overrun). Try to manage it.
                        //DONE: before reading, delete RTP header
                        //We take away the RTP received header
                        rtp_header_received = (rtp_hdr_t *) socket_buffer;
                        char *audio_buffer = (char *) (rtp_header_received + 1);
                        //The following operation was not correct:
                        //socket_buffer = socket_buffer + sizeof(struct rtp_hdr_t *);
                        //TODO check if we need a variable to determine if we need to start with
                        // the same external_seq and external_ts
                        /**Logic of RTP header**/
                        //Check that the sequence number received is the good one
                        //TODO take into account silences?

                        memcpy(pointer_to_write, audio_buffer,requestedFragmentSize);
                        current_blocks_in_cb++;
                        number_of_block++;

                        first_packet_received = 1;
                        external_seq = ntohs(rtp_header_received->seq);
                        external_ts = ntohl(rtp_header_received->ts);

                    }
                }
            }
        }
    }


    /** Second loop **/
    while(1) {
        FD_ZERO(&read_block);
        FD_ZERO(&write_block);
        FD_SET(sndCardDesc, &read_block);
        FD_SET(socketDesc, &read_block);
        calculate_remaining_time(sndCardDesc,&remaining_time);
        //DONE: should we set the 10ms timer? -> Yes, to insert silences in the circular buffer, but only in the steady regime.
        timersub(&remaining_time,&ten_ms_time,&time);

        if(current_blocks_in_cb > 0) {
            FD_SET(sndCardDesc, &write_block);
            ready = select(FD_SETSIZE, &read_block, &write_block, NULL, &time);
        } else {
            ready = select(FD_SETSIZE, &read_block, NULL, NULL, &time);

        }
       // printf("%ld",remaining_time.tv_usec);
        //TODO check if this works in leganes
        if(ready < 0){
            printf("An error ocurred related with the timer \n");
            exit(EXIT_FAILURE);
            fflush(stdout);
        } else if (ready == 0) {
            //The timer has expired. TODO: try to manage it.
            //DONE: insert a silence audio fragment and update the expected TS value as if the packet was effectively received.
            char* pointer_to_write;
            if((pointer_to_write = cbuf_pointer_to_write(circularBuffer)) != NULL){
                memcpy(pointer_to_write, silence_buffer, requestedFragmentSize);
                current_blocks_in_cb++;
                if(verbose) {
                    printf("t");
                    printf("~");
                    silences_detected++;
                }
                //printf("%ld",remaining_time.tv_usec);
                fflush(stdout);
                external_ts+=frame_packet;
            } else {
                printf("\n Circular buffer is full while the timer is expired?");
                printf("**");
                printf("%d",current_blocks_in_cb);
                printf("**");
                exit(EXIT_FAILURE);
            }

           // printf("The timer has been reached");
        } else {

            //The very first step is to check if there is any block in the circular buffer.
            //If it is that the case, we must request a write operation in the soundcard.
            if (FD_ISSET(sndCardDesc, &read_block) == 1) {
                if (first_10_secs) {
                    first_10_secs = 0;
                    gettimeofday(&first_read_time, NULL);
                }
                gettimeofday(&current_time, NULL);
                timersub(&current_time, &first_read_time, &diff_time);
                //DONE se empieza desde el cero en ts y seq o no hace falta? -> sí
                //Data is available to be sent to the multicast address.
                //Now we should fill in the RTP header
                rtp_header = (rtp_hdr_t *) paquete_red;
                rtp_header->version = RTP_VERSION; //Protocol version
                rtp_header->p = 0; //Padding flag
                rtp_header->x = 0; //Header extension flag
                rtp_header->cc = 1; //CSRC count
                rtp_header->m = 0; //Marker bit
                rtp_header->pt = payload; //Payload type.
                rtp_header->seq = htons(local_seq);
                rtp_header->ts = htonl(local_ts);
                rtp_header->ssrc = htonl(ssrc);
                record_buffer = (char *) (rtp_header + 1);
                record(sndCardDesc, requestedFragmentSize);
                /* Using sendto to send information. Since I've bind the socket, the local (source)
                 * port of the packet is fixed. In the rem structure I set the remote (destination) address and port  */

                bool isSilence = ((diff_time.tv_sec >= TIME_10_SECONDS) && isFragmentSilence(record_buffer));
                if (!isSilence) {
                    if ((result = sendto(socketDesc, paquete_red, requestedFragmentSize + 12, /* flags*/  0,
                                         (struct sockaddr *) &remToSendSAddr, sizeof(remToSendSAddr))) < 0) {
                        printf("sendto error\n");
                    } else {
                        if(verbose) {
                            packets_sent++;
                            printf(".");
                            fflush(stdout);
                        }
                        local_seq++;
                    }
                } else {
                    if(verbose) {
                        silences_recorded++;
                        printf("_");
                        fflush(stdout);
                    }
                    //Even if it is a silence, we must update the TS:
                }
                local_ts += frame_packet;
                memset(paquete_red, 0,(size_t )(12 + requestedFragmentSize));
            }
            if (FD_ISSET(socketDesc, &read_block) == 1) {
                //Data from another audioc entity has arrived at the socket.
                if ((result = recvfrom(socketDesc, socket_buffer, requestedFragmentSize + 12, 0,
                                       (struct sockaddr *) &remToRecvSAddr, &sockAddrInLength)) < 0) {
                    printf ("recvfrom error\n");
                } else {
                    //printf("Audio received\n");
                    if(verbose) {
                        printf("+");
                        packets_received++;
                        fflush(stdout);
                    }
                   // socket_buffer[result] = 0; /* convert to 'string' by appending a 0 value (equal to '\0') after the last received character */
                    char* pointer_to_write;
                    /**We need to do this before checking if there is space in the circular buffer **/
                    rtp_header_received = (rtp_hdr_t *) socket_buffer;
                    char* audio_buffer = (char *) (rtp_header_received + 1);
                    uint16_t new_external_seq = ntohs(rtp_header_received->seq);
                    uint32_t new_external_ts = ntohl(rtp_header_received->ts);
                    if((pointer_to_write = cbuf_pointer_to_write(circularBuffer)) != NULL){
                        //TODO: pointer_to_write could be NULL if there are no free slots at the buffer (overrun). Try to manage it.
                        //DONE: before reading, delete RTP header
                        //We take away the RTP received header

                        //The following operation was not correct:
                        //socket_buffer = socket_buffer + sizeof(struct rtp_hdr_t *);
                        /**Logic of RTP header**/
                        //Check that the sequence number received is the good one

                       /* printf("%d",external_ts);
                        printf("***");
                        printf("%d",new_external_ts);*/

                        if((external_seq + 1) == new_external_seq) {
                            //We check the timestamp to see if there have been silences
                            if((external_ts + frame_packet) == new_external_ts) {
                                memcpy(pointer_to_write, audio_buffer, requestedFragmentSize);
                                current_blocks_in_cb++;
                            } else if((external_ts + frame_packet) < new_external_ts + 1) {
                                //Calculate the number of silences to be inserted
                                int number_of_silences = ((new_external_ts + requestedFragmentSize) - external_ts) / frame_packet;
                                int i = 0;
                                for (i = 0; i < number_of_silences; i++) {
                                    if (i != 0) {
                                        if ((pointer_to_write = cbuf_pointer_to_write(circularBuffer)) == NULL) {
                                            break;
                                        }
                                    }
                                    memcpy(pointer_to_write, silence_buffer,requestedFragmentSize);
                                    current_blocks_in_cb++;
                                    if(verbose) {
                                        printf("~");
                                        silences_detected++;
                                        fflush(stdout);
                                    }
                                    /**Dont try to insert more than needed**/
                                    if(current_blocks_in_cb >= number_of_block) {break;}
                                }
                            }
                        /**Check if by any case, there has been some lost packets and we have reproduce silences **/
                        } else if((external_seq + 1) < new_external_seq && first_packet_received){
                            //Check how many packets have been lost
                            //int lost_packets = rtp_header_received->seq - (external_seq);
                            if(first_packet_received == 1) {
                                int number_of_silences =
                                        ((new_external_ts + requestedFragmentSize) - external_ts) / frame_packet;
                                int i = 0;
                                /**Print number of losts*/
                                for (i = 0; i < (new_external_seq - (external_seq + 1)); i++) {
                                    if (verbose) {
                                        printf("x");
                                        losts_detected++;
                                    }
                                }
                                for (i = 0; i < number_of_silences; i++) {
                                    if (i != 0) {
                                        if ((pointer_to_write = cbuf_pointer_to_write(circularBuffer)) == NULL) {
                                            break;
                                        }
                                    }
                                    memcpy(pointer_to_write, silence_buffer, requestedFragmentSize);
                                    current_blocks_in_cb++;
                                    /**Dont try to insert more than needed**/
                                    if (current_blocks_in_cb >= number_of_block) { break; }
                                    if (verbose) {
                                        printf("~");
                                        silences_detected++;
                                    }
                                }
                            }
                            memcpy(pointer_to_write, audio_buffer, requestedFragmentSize);
                            current_blocks_in_cb++;
                            first_packet_received = 1;

                            fflush(stdout);
                        }
                    } else {
                        printf("Circular buffer is full ***%d***", current_blocks_in_cb);
                        fflush(stdout);
                    }
                    /**We save the new external_seq and the external_ts **/
                    external_seq = new_external_seq > external_seq ? new_external_seq : external_seq;
                    external_ts = new_external_ts > external_ts ? new_external_ts : external_ts;
                    //Now we go back to the initial memory position of the socket buffer in which next received packet should be stored.
                    //We can clean up the socket bufferingTime
                    memset(socket_buffer, 0, (size_t )(12+requestedFragmentSize));

                }
            }

            if(FD_ISSET(sndCardDesc, &write_block) == 1) {
                //printf("Audio played");
                play(sndCardDesc,requestedFragmentSize);
            }
        }
    }
//In order to avoid some compiler warnings:

    return 0;
}

void record (int descSnd, int fragmentSize)
{

    int bytesRead;

    if (record_buffer == NULL) {
        printf("Could not reserve memory for audio data.\n");
        exit (1); /* very unusual case */
    }

    //printf("Recording in blocks of %d bytes... :\n", fragmentSize);

    if ((bytesRead = read(descSnd, record_buffer, fragmentSize)) < 0) {
        printf("Error reading from soundcard, error: %s\n", strerror(errno));
        exit(1);
    }
    if (bytesRead != fragmentSize) {
        printf ("Recorded a different number of bytes than expected (recorded %d bytes, expected %d)\n", bytesRead, fragmentSize);
    }
    if(verbose) { recorded_packets++; }

}

void play (int descSnd, int fragmentSize)
{
    /** To identify if it has been reproduced some kind of audio **/
    fflush(stdout);
    int bytesWrite;
    //printf("%d",cbuf_has_block(circularBuffer));
    if (cbuf_has_block(circularBuffer) == 1) {
        void *pointer_to_read;
        if ((pointer_to_read = cbuf_pointer_to_read(circularBuffer)) != NULL) {
            //TODO: what if there is nothing to read? In that case, pointer_to_read will be NULL. Try to manage it.
            if ((bytesWrite = write(descSnd, pointer_to_read, fragmentSize)) < 0)
            {
                printf("Error writing to soundcard, error: %s\n", strerror(errno));
                exit(1);
            }

            if (bytesWrite != fragmentSize)
            {
                printf ("Played a different number of bytes than expected "
                        "(played %d bytes, expected %d; exiting)\n", bytesWrite, fragmentSize);
                exit(1);
            }
            current_blocks_in_cb--;
            if(verbose) {
                if(is_first_playout == 0) {
                    gettimeofday(&first_playout, NULL);
                    is_first_playout = 1;
                }
                inserted_packets++;
                gettimeofday(&last_playout, NULL);
                printf("-");
            }
            fflush(stdout);
        } else {
            printf("Null pointer \n");
        }
    }
}
/**Function to calculate the reamining time**/
void calculate_remaining_time(int sndCardDesc, struct timeval *remaining_time) {
    //We need to know how many bytes away from exhausting the buffer we are, so that we can compute the remaining time and update the timer.
    int bytes_exhausted = 0;
    ioctl(sndCardDesc, SNDCTL_DSP_GETODELAY, &bytes_exhausted);
    //TODO Check this in leganes
    //if (bytes_exhausted > 0) {
        //Good case.
        //DONE: check sample_size / 8, since the function wants bytes and no bits
        int remaining_msecs = floor((bytes_exhausted * 1000 / (rate * 1 * (sample_size / 8))));
        remaining_msecs = remaining_msecs <= 0 ? 5 : remaining_msecs;
        int remaining_msecs_circular = floor(
                (current_blocks_in_cb * requestedFragmentSize * 1000 / (rate * 1 * (sample_size / 8))));

        remaining_msecs = remaining_msecs + remaining_msecs_circular;
        remaining_msecs = remaining_msecs < TIME_TIMER ? TIME_TIMER : remaining_msecs;
        remaining_time->tv_usec = (remaining_msecs * 1000);
}

bool isSampleSilence8(uint8_t sample){
    return (sample > (SILENCE_8 - MA_8) && sample < (SILENCE_8 + MA_8));
}

bool isSampleSilence16(int16_t sample) {
    return (sample > (SILENCE_16 - MA_16) && sample < (SILENCE_16 + MA_16));
}

bool isFragmentSilence(char *buffer) {
    int number_samples = requestedFragmentSize* 8/sample_size;
    int number_silences = 0;
    int i = 0;
    char* pointer = buffer;
    if(sample_size == 8) {
        for (i = 0; i < number_samples; i++) {
            uint8_t sample = (uint8_t) *(pointer);
            number_silences += isSampleSilence8(sample) ? 1 : 0;
            pointer = pointer + sizeof(uint8_t);
        }
    } else {
        for (i = 0; i < number_samples; i++) {
            int16_t sample = (int16_t) be16toh(*(pointer));
            number_silences += isSampleSilence16(sample) ? 1 : 0;
            pointer = pointer + sizeof(int16_t);
        }
    }
    return (number_silences * 100 / number_samples) > PMA;
}