/*******************************************************/
/* configureSndcard.h */
/*******************************************************/

#ifndef SOUND_H
#define SOUND_H

#include <stdbool.h>

enum formats {
    U8 = 8,         /* unsigned 8 bits */ 
    // S16_LE = 16     /* signed 16 bits, little endian */
    S16_BE = 32     /* signed 16 bits, big endian */
};


/* This function changes the configuration for the descSnd provided to the parameters 
 * stated in dataSndQuality. 
 * If an error is found in the configuration of the soundcard, the process is stopped 
 * and an error message reported.
 * It always opens the sound card, and return the corresponding soundcard descriptor */
void configSndcard (int *descSnd, /* Does not use the initial value of the variable.
                                    Returns number of soundcard descriptor */
        int *bits,              /* Receives the number of bits, transforms to formats in use (see 'enum formats').
                                    E.g.: 16 bits is transformed into S16_BE
                                    Returns format actually configured at the soundcard 
                                    (may be different than requested). */
        int *channelNumber,     /* Receives the requested number of channels. 
                                    Returns number of channels (1 or 2) actually 
                                    configured at the soundcard (may be different than requested) */
        int *rate,              /* Receives the requested sampling rate. 
                                    Returns sampling rate (Hz) actually configured at 
                                    the soundcard (may be different than requested) */
	    int *fragmentSize,      /* Receives the requested fragment size in bytes.
                                   Returns the actual fragment size (always a power of 2 
                                   value equal or lower to the requested value). */
        bool duplexMode         /* Receives a request to configure DUPLEX mode (true) or not. 
                                   Returns same value.*/
	);

/* Configures volume for descSnd. 
 * If 'stereo' is set to 1, it configures both channels, otherwise configures just one. 
 * Receives a number in the [0..100] range.
 * The function returns the volume actually configured in the device after performing 
 * the operation (could be different than requested).
 * If an error is found in the configuration of the soundcard, the process is stopped 
 * and an error message reported. */
int configVol (int stereo, int descSnd, int vol);

/* prints fragment size configured for sndcard */
void printFragmentSize (int descriptorSnd);

#endif /* SOUND_H */


